module github.com/notmeta/osrs.cx

require (
	github.com/bwmarrin/discordgo v0.19.0
	github.com/go-redis/redis v6.15.2+incompatible
	github.com/hashicorp/go-version v1.2.0 // indirect
	github.com/mitchellh/gox v1.0.1 // indirect
	golang.org/x/crypto v0.0.0-20190701094942-4def268fd1a4 // indirect
	golang.org/x/net v0.0.0-20190628185345-da137c7871d7 // indirect
	golang.org/x/sys v0.0.0-20190626221950-04f50cda93cb // indirect
	golang.org/x/text v0.3.2 // indirect
	golang.org/x/tools v0.0.0-20190706070813-72ffa07ba3db // indirect
)
